#include <iostream>

using namespace std;

int main() {
    cout << "===== PROGRAM PENGHITUNG BILANGAN PRIMA PADA FIBONACCI =====" << endl;
    cout << "============================================================" << endl << endl;

    int input;
    cout << "Input: ";
    cin >> input;

    int fibonacci[input], prima[input];

    // fibonacci
    cout << endl << "Output:" << endl;
    int fibo1 = 0, fibo2 = 1, temp;
    for(int i = 0; i < input; i++) {
        fibonacci[i] = fibo2;
        cout << fibo2 << " ";

        temp = fibo1 + fibo2;
        fibo1 = fibo2;
        fibo2 = temp;

    }

    // prima
    int count = 0, status, mulai;
    for(int i = 0; i < input; i++) {
        mulai = 1;
        status = 0;
        while(mulai <= fibonacci[i]) {
            if(fibonacci[i] % mulai == 0) {
                status++;
            }
            mulai++;
        }

        if(status == 2) {
            count++;
            prima[count - 1] = fibonacci[i];
        }
    }

    
    if(count == 0) cout << endl << "Tidak ada bilangan prima";
    else {
        cout << endl << "Terdapat " << count << " bilangan prima, yaitu: ";
        for(int i = 0; i < count; i++) {
            cout << prima[i] << " ";
        }
    }
    

    return 0;
}