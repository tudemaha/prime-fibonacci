#include <iostream>
using namespace std;

int fib(int n){
	if(n == 0 || n == 1){
		return n;
	}else{
		return fib(n - 1) + fib(n - 2);
	}
}

int main(){
	int angka, hasil = 1, a;
	cout << "Bilangan fibonacci yang ditampilkan: ";
	cin >> angka;

	// fibonacci
    cout << endl << "Output:" << endl;
	int fibonacci[angka], prima[angka];
	for(int i=0; i<angka; i++){
		fibonacci[i] = fib(hasil);
		cout << fibonacci[i] << " ";
		hasil++;
	}

	// prima
    int count = 0, status, mulai;
    for(int i = 0; i < angka; i++) {
        mulai = 1;
        status = 0;
        while(mulai <= fibonacci[i]) {
            if(fibonacci[i] % mulai == 0) {
                status++;
            }
            mulai++;
        }

        if(status == 2) {
            count++;
            prima[count - 1] = fibonacci[i];
        }
    }

    
    if(count == 0) cout << endl << "Tidak ada bilangan prima";
    else {
        cout << endl << "Terdapat " << count << " bilangan prima, yaitu: ";
        for(int i = 0; i < count; i++) {
            cout << prima[i] << " ";
        }
    }
}
